import { constants } from '../constants/constants';

const initialState = {
    loading: false,
    msgError: null
}

export const uiReducer = ( state = initialState, action) => {
    switch (action.type) {
        case constants.uiSetError:
            return {
                ...state,
                msgError: action.payload
            }

        case constants.uiRemoveError:
            return {
                ...state,
                msgError: null
            }

        case constants.uiStartLoading: 
            return {
                ...state,
                loading: true
            }

        case constants.uiFinishLoading: 
            return {
                ...state,
                loading: false
            }

        default:
            return state;
    }
}
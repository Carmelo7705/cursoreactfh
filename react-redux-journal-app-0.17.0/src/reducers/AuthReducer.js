import { constants } from "../constants/constants";

/**
    state empty if user unauthorized, if user auth, state with uid, name.
 */

export const authReducer = (state = {}, action) => {

    switch (action.type) {
        case constants.login:
            return {
                uid: action.payload.uid,
                name: action.payload.displayName
            }
        case constants.logout:
            return {}
    
        default:
            return state;
    }

};
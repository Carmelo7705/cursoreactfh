import { constants } from '../constants/constants';

export const setError = ( err ) => {
    return {
        type: constants.uiSetError,
        payload: err
    }
}

export const removeError = () => {
    return {
        type: constants.uiRemoveError,
    }
}

export const uiStartLoading = () => {
    return {
        type: constants.uiStartLoading,
    }
}

export const uiFinishLoading = () => {
    return {
        type: constants.uiFinishLoading
    }
}
import Swal from 'sweetalert2';

import { constants } from "../constants/constants";
import { firebase, googleAuthProvider } from "../firebase/firebase-config";
import { uiStartLoading, uiFinishLoading } from "./ui";

export const startLoginEmailPassword = ( email, password ) => {
    return ( dispatch ) => {
        console.log(email, password);
        dispatch( uiStartLoading() )

        firebase.auth().signInWithEmailAndPassword( email, password )
            .then( ({ user }) => {
                console.log( user );
                dispatch( login( user.uid, user.displayName ) );
                dispatch( uiFinishLoading() )
            })
            .catch(err => {
                dispatch( uiFinishLoading() );
                Swal.fire( 'Error',  err.message );
            });
    }
}

export const startGoogleLogin = () => {
    return ( dispatch ) => {
        firebase.auth().signInWithPopup( googleAuthProvider )
            .then( ({ user }) => {
                dispatch( login(user.uid, user.displayName) );
            });
    }
}

export const startRegister = ( email, password, name ) => {
    return ( dispatch ) => {

        firebase.auth().createUserWithEmailAndPassword( email, password )
            .then( async ({ user }) => {
                await user.updateProfile({ displayName: name });
                console.log(user)
                
                dispatch( login( user.uid, user.displayName ) );
            })
            .catch(err => console.log( err ));
    }
}

export const login = (uid, displayName) => {
    return {
        type: constants.login,
        payload: {
            uid, 
            displayName
        }
    }
}

export const startLogout = () => {
    return async ( dispatch ) => {
        await firebase.auth().signOut();

        dispatch( logout() );
    }
}

export const logout = () => {
    return {
        type: constants.logout
    }
}
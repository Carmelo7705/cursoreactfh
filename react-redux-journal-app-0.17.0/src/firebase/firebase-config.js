import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyDomvUAzDXrO7gmhpLTqGArDCI3IS5wPYg",
    authDomain: "journal-app-b0258.firebaseapp.com",
    projectId: "journal-app-b0258",
    storageBucket: "journal-app-b0258.appspot.com",
    messagingSenderId: "629873490241",
    appId: "1:629873490241:web:c1735d20e476579c4249c0",
    measurementId: "G-LS3N1Q39XT"
};

firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
const googleAuthProvider = new firebase.auth.GoogleAuthProvider();

export {
    db,
    googleAuthProvider,
    firebase
}
import React from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import validator from 'validator';

import { useForm } from '../../hooks/useForm';
import { removeError, setError } from '../../actions/ui';
import { startRegister } from '../../actions/auth';

export const RegisterScreen = () => {

	const dispatch = useDispatch();
	const { msgError } = useSelector( state => state.ui );

	const [ formValues, handleInputChange ] = useForm({
		name: '', email: '', password: '', password2: ''
	});

	const { name, email, password, password2 } = formValues;

	const handleRegister = (e) => {
		e.preventDefault();

		if(isFormValid()) {
      dispatch ( startRegister(email, password, name) );
		}

		cleanFields();
		
	}

	const isFormValid = () => {

		if(name.trim().length === 0) {
			dispatch( setError('El nombre del formulario no puede estar vacío.') );
			return false;
		}

		if(!validator.isEmail( email )) {
			dispatch( setError('El email no posee un formato válido.') );
			return false;
		}

		if(password !== password2 || password.length < 5) {
			dispatch( setError('Las contraseñas no coinciden o es menor a 5 carácteres.') );
			return false;
		}
		
		dispatch( removeError() );
		return true;
	}

	const cleanFields = () => {
		formValues.email = '';
		formValues.name = '';
		formValues.password = '';
		formValues.password2 = '';
	}

    return (
        <>
            <h3 className="auth__title">Register</h3>

            <form onSubmit={ handleRegister }>

							{
								msgError && 
									(
										<div className="auth__alert-error">
											{ msgError }
										</div>
									)
							}

                <input 
                    type="text"
                    placeholder="Name"
                    name="name"
                    className="auth__input"
										onChange={ handleInputChange }
										value={ name }
                    autoComplete="off"
                />

                <input 
                    type="text"
                    placeholder="Email"
                    name="email"
                    className="auth__input"
										onChange={ handleInputChange }
										value={ email }
                    autoComplete="off"
                />

                <input 
                    type="password"
                    placeholder="Password"
                    name="password"
                    className="auth__input"
										onChange={ handleInputChange }
										value={ password }
                />

                <input 
                    type="password"
                    placeholder="Confirm password"
                    name="password2"
                    className="auth__input"
										onChange={ handleInputChange }
										value={ password2 }
                />


                <button
                    type="submit"
                    className="btn btn-primary btn-block mb-5"
                >
                    Register
                </button>

               

                <Link 
                    to="/auth/login"
                    className="link"
                >
                    Already registered?
                </Link>

            </form>
        </>
    )
}
